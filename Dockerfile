FROM python:3.6-alpine3.6

ENV BUILD_DEPS alpine-sdk coreutils ghc gmp libffi linux-headers musl-dev wget zlib-dev curl
ENV PERSISTENT_DEPS \
    graphviz \
    openjdk8-jre \
    sed \
    ttf-droid \
    ttf-droid-nonlatin \
    nginx
ENV EDGE_DEPS cabal

# ENV PLANTUML_VERSION 1.2017.18
# ENV PLANTUML_DOWNLOAD_URL https://sourceforge.net/projects/plantuml/files/plantuml.$PLANTUML_VERSION.jar/download

ENV PANDOC_VERSION 2.1.1
ENV PANDOC_DOWNLOAD_URL https://hackage.haskell.org/package/pandoc-$PANDOC_VERSION/pandoc-$PANDOC_VERSION.tar.gz
ENV PANDOC_ROOT /usr/local/pandoc

ENV PATH $PATH:$PANDOC_ROOT/bin

# Create Pandoc build space
RUN mkdir -p /pandoc-build
WORKDIR /pandoc-build

# Install/Build Packages
RUN echo -e "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.6/main\n\
https://mirror.tuna.tsinghua.edu.cn/alpine/v3.6/community" > /etc/apk/repositories && \
    apk upgrade --update 

RUN apk add --no-cache --virtual .build-deps $BUILD_DEPS
RUN apk add --no-cache --virtual .persistent-deps $PERSISTENT_DEPS
RUN apk add --no-cache --virtual .edge-deps $EDGE_DEPS -X http://dl-cdn.alpinelinux.org/alpine/edge/community

RUN curl -L "http://7u2nod.com1.z0.glb.clouddn.com/plantuml.jar" -o /lib/plantuml.jar && \
    echo -e "#!/bin/sh -e\njava -jar /lib/plantuml.jar -Djava.awt.headless=true \"\$@\"" > /usr/local/bin/plantuml && \
    chmod +x /usr/local/bin/plantuml && \
RUN curl -fsSL "$PANDOC_DOWNLOAD_URL" | tar -xzf - && \
        ( cd pandoc-$PANDOC_VERSION && cabal update && cabal install --only-dependencies && \
        cabal configure --prefix=$PANDOC_ROOT && \
        cabal build && \
        cabal copy && \
        cd .. ) && \
    rm -Rf pandoc-$PANDOC_VERSION/ && \
    rm -Rf /root/.cabal/ /root/.ghc/ && \
    rmdir /pandoc-build && \
    set -x; \
    addgroup -g 82 -S www-data; \
    adduser -u 82 -D -S -G www-data www-data && \
    mkdir -p /var/docs && \
    apk del .build-deps .edge-deps


RUN pip3 install sphinx \
            sphinxcontrib-httpdomain \
            sphinxcontrib-plantuml \
            sphinxcontrib-websupport \
            cloud_sptheme \
            pandoc \
            nbsphinx \
            jupyter_sphinx_theme \
            -i https://pypi.douban.com/simple

WORKDIR /app
